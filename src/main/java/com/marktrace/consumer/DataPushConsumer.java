package com.marktrace.consumer;

import com.alibaba.fastjson.JSON;
import com.marktrace.dao.MemberServiceObjLockHisDao;
import com.marktrace.dao.MemberServiceObjMonitorHisDao;
import com.marktrace.entity.MemberServiceObjLockHis;
import com.marktrace.entity.MemberServiceObjMonitorHis;
import com.marktrace.service.HttpClient;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * DataPushConsumer
 *
 * @author ck
 * @date 2018年12月29日14:33:23
 */
@Component
public class DataPushConsumer {
    @Autowired
    private MemberServiceObjMonitorHisDao memberServiceObjMonitorHisDao;
    @Autowired
    private MemberServiceObjLockHisDao memberServiceObjLockHisDao;
    @Autowired
    private HttpClient httpClient;

    @Value("${wxUrl}")
    private String wxUrl;

    @KafkaListener(topics = "${kafksTopic}" )
    public void listen(ConsumerRecord<?,String> record) {
        String value = record.value();
        if(!StringUtils.isEmpty(value)){
            Map<String,Object> resultMap = (Map<String, Object>) JSON.parse(value);
            if(!resultMap.isEmpty()){
                int alarmType = Integer.valueOf(resultMap.get("AlarmType").toString());
                String hLabelNo = resultMap.get("TagId").toString();
                //AlarmType，1：锁车异常，2：接警布控
                MemberServiceObjMonitorHis m = memberServiceObjMonitorHisDao.findByHlabelno(hLabelNo);
                MemberServiceObjLockHis lockLog = memberServiceObjLockHisDao.findByHLabelNo(hLabelNo);
                if(alarmType == 1){
                    wxPush(resultMap);
                    lockInput(lockLog,resultMap);
                }else if(alarmType == 2){
                    alarmInput(m,resultMap);
                }
            }
        }
    }

    /**
     * 修改平台接警布控
     * 报警日志记录：标签号作为唯一标识，存在则修改，不存在则新增
     * @param m
     */
    public void alarmInput(MemberServiceObjMonitorHis m,Map<String,Object> resultMap){
        if(m == null){
            m = new MemberServiceObjMonitorHis();
            m.setHlabelno(resultMap.get("TagId").toString());
            m.setEqno(resultMap.get("DeviceId").toString());
        }
        m.setMemberserviceobjmonitorhischeckstatus(1);
        m.setMemberserviceobjmonitorhisaddtime(resultMap.get("TagTime").toString());
        memberServiceObjMonitorHisDao.save(m);
    }
    /**
     * 调用微信接口进行模板消息推送
     */
    public void wxPush(Map<String,Object> resultMap){
        String url = wxUrl+"?TagId={TagId}&DeviceId={DeviceId}&TagTime={TagTime}";
        Map<String,Object> map = new HashMap<>();
        map.put("TagId",resultMap.get("TagId"));
        map.put("DeviceId",resultMap.get("DeviceId"));
        map.put("TagTime",resultMap.get("TagTime"));
        String resultMsg = httpClient.clientForGet(url,map);
        System.out.println(resultMsg);
    }

    /**
     * 修改平台锁车日志
     */
    public void lockInput(MemberServiceObjLockHis m, Map<String,Object> resultMap){
        if(m == null){
            m = new MemberServiceObjLockHis();
            m.setHLabelNo(resultMap.get("TagId").toString());
            m.setEqno(resultMap.get("DeviceId").toString());
        }
        m.setMemberServiceObjLockHisCheckStatus(1);
        m.setMemberServiceObjLockHisAddTime(resultMap.get("TagTime").toString());
        memberServiceObjLockHisDao.save(m);
    }
}
