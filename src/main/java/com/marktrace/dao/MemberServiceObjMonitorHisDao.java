package com.marktrace.dao;

import com.marktrace.entity.MemberServiceObjMonitorHis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * MemberServiceObjMonitorHisDao
 *
 * @author ck
 * @date 2018/12/27
 */
@Repository
public interface MemberServiceObjMonitorHisDao extends JpaRepository<MemberServiceObjMonitorHis,Integer> {
    /**
     * 根据标签号记录
     * @param hlabelno
     * @return
     */
    MemberServiceObjMonitorHis findByHlabelno(String hlabelno);
}
