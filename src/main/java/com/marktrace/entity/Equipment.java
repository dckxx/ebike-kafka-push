package com.marktrace.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="hEquipment")
public class Equipment {
    @Id
    @GeneratedValue
    private int eqid;
    @Column(name = "eqno")
    private String eqno;
}
