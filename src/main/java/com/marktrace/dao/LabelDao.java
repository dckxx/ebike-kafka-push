package com.marktrace.dao;

import com.marktrace.entity.Label;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LabelDao extends JpaRepository<Label,Integer> {
}
