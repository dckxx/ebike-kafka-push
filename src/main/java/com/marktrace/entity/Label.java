package com.marktrace.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="hLabel")
public class Label {
    @Id
    @GeneratedValue
    private int hLabelId;
    @Column(name = "hLabelNo")
    private String hLabelNo;
}
