package com.marktrace.dao;

import com.marktrace.entity.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipmentDao extends JpaRepository<Equipment,Integer> {

}
