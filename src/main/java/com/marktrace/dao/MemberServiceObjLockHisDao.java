package com.marktrace.dao;

import com.marktrace.entity.MemberServiceObjLockHis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * MemberServiceObjLockHisDao
 *
 * @author ck
 * @date 2018/12/27
 */
@Repository
public interface MemberServiceObjLockHisDao extends JpaRepository<MemberServiceObjLockHis,Integer> {
    /**
     * 根据标签号查询记录
     * @param hlabelno
     * @return
     */
    MemberServiceObjLockHis findByHLabelNo(String hlabelno);
}
