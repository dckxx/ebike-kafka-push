package com.marktrace.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class HttpClient {
    /**
     * get请求
     * @param url：请求地址，参数写在url后面，形式如 param={param}
     * @param map：请求参数，key跟url的参数名保持一致
     * @return
     */
    public String clientForGet(String url, Map<String,Object> map){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url,String.class,map);
        return responseEntity.getBody();
    }

    /**
     * post请求
     * @param url：请求地址
     * @param map：请求参数
     * @return
     */
    public String clientForPost(String url, Map<String,Object> map){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url,map,String.class);
        return responseEntity.getBody();
    }
}
