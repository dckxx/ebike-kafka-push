package com.marktrace;

import com.marktrace.dao.MemberServiceObjMonitorHisDao;
import com.marktrace.entity.MemberServiceObjMonitorHis;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class EbikeKafkaPushApplicationTests {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Autowired
    private MemberServiceObjMonitorHisDao memberServiceObjMonitorHisDao;
    @Test
    public void contextLoads() {
        String data = "{\n" +
                "    \"ChannelNo\": \"1\",\n" +
                "    \"DeviceId\": \"100000000000009\",\n" +
                "    \"Incentive_Address\": \"0\",\n" +
                "    \"TagIOType\": \"0\",\n" +
                "    \"TagId\": \"16102356\",\n" +
                "    \"TagRssi\": \"-87\",\n" +
                "    \"TagStay\": \"0\",\n" +
                "    \"TagTime\": \"2018-11-06 15:48:16\",\n" +
                "    \"TagType\": \"20\",\n" +
                "    \"AlarmType\": \"1\"\n" +
                "}";
        kafkaTemplate.send("lockcar_loction",data);
        log.info("已模拟一条数据");
    }

    @Test
    public void add(){
        MemberServiceObjMonitorHis m = new MemberServiceObjMonitorHis();
        m.setEqno("123");
        m.setHlabelno("16102356");
        m.setMemberserviceobjmonitorhisaddtime("");
        m.setMemberserviceobjmonitorhischeckstatus(1);
        memberServiceObjMonitorHisDao.save(m);
    }

}

