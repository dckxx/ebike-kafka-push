# ebike-kafka-push

#### 介绍
ebike-kafka-push是一个接收kafka数据并处理的项目

#### 软件架构
该项目使用springboot2.1.1，其中用到spring-boot-starter-web中的RestTemplate来调用远程url，采用JPA访问数据库，对kafka接收到的数据进行处理。


#### 安装教程

无

#### 使用说明

1. 直接运行EbikeKafkaPushApplication即可（如需发送数据，请使用test）


