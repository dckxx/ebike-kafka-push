package com.marktrace.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * MemberServiceObjMonitorHis
 *
 * @author ck
 * @date 2018年12月29日14:59:59
 */
@Data
@Entity
@Table(name="memberserviceobjmonitorhis")
public class MemberServiceObjMonitorHis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int memberserviceobjmonitorhisid;
    @Column(name="hLabelNo")
    private String hlabelno;
    @Column(name="eqno")
    private String eqno;
    @Column(name="memberServiceObjMonitorHisAddTime")
    private String memberserviceobjmonitorhisaddtime;
    @Column(name="memberServiceObjMonitorHisCheckStatus")
    private int memberserviceobjmonitorhischeckstatus;

}
