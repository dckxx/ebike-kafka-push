package com.marktrace.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * MemberServiceObjLockHis
 *
 * @author ck
 * @date 2018年12月29日14:58:18
 */
@Data
@Entity
@Table(name = "memberserviceobjlockhis")
public class MemberServiceObjLockHis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int memberServiceObjLockHisId;
    @Column(name="hLabelNo")
    private String hLabelNo;
    @Column(name="eqno")
    private String eqno;
    @Column(name="memberServiceObjLockHisAddTime")
    private String memberServiceObjLockHisAddTime;
    @Column(name="memberServiceObjLockHisCheckStatus")
    private int memberServiceObjLockHisCheckStatus;

}
