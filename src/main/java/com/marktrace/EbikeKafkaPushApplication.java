package com.marktrace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EbikeKafkaPushApplication {

    public static void main(String[] args) {
        SpringApplication.run(EbikeKafkaPushApplication.class, args);
    }

}

